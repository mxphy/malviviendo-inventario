# rage.mp inventory by FR0Z3NH34R7 (God mode)

## Instructions

1.  Run the command `npm install` to install the npm dependencies.
2.  Run the command `npm run build` to build the app.
3.  Copy the files from the "build" folder to the folder of your server. Do not forget to check the paths of the js files in index.html.

If you want to add images to the items, go to ui/app/images.js

## Properties

0 = no drop, no use, no read, no show
1 = use, drop, no read, no show
2 = no use, no drop, read, show

## Credits

- Materialize Framework
- React.js

Developed with ♥ by FR0Z3NH34R7 for Vespucci RP.
