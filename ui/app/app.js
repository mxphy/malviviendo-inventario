import '../styles/app.scss'

import React from 'react'
import ReactDOM from 'react-dom'
import Inventory from './components/Inventory'
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            time: new Date(),
            message: 'default message',
            tab: 0,
            data: [],
        }
    }
    
    componentDidMount() {
        
        EventManager.addHandler('onReceiveObjects', this.onReceiveObjects.bind(this))
    }

    componentWillUnmount() {
       
        EventManager.removeHandler('onReceiveObjects', this.onReceiveObjects)
    }

    onReceiveObjects(data) {
        console.log("Llamando onReceiveObjects")
        console.log(data)
        this.setState({data});
        console.log("Seteando estado")
        console.log("Estado: "+this.state.data);
        for (var x=0; x < data.length; x++)
            {
                console.log(this.state.data[x].Slot);
                if(this.isSlotOcuppied(this.state.data[x].Slot, x))
                {
                    console.log("Slot ocupado, reordenando.")
                    let recipesCopy = JSON.parse(JSON.stringify(this.state.data))
                    recipesCopy[x].Slot = parseInt(this.getEmptySlot()); 
                    this.setState({
                        data:recipesCopy 
                    });
                }
            }
            console.log(data)
    }
    isSlotOcuppied(slot, item) {
        for (var x=0; x < this.state.data.length; x++)
        {
            if(this.state.data[x].Slot === slot && item !== x)
            {
                return 1;
            }
         }
         return 0;
    }
    getEmptySlot() {
        for (var x=0; x < 24; x++)
        {
            if(!this.isSlotOcuppied(x))
            {
                return x;
            }
        }
        return -1;
    }

    // send current url to client
    click() {
        let currentUrl = window.location.pathname        
        mp.trigger('showUrl', currentUrl)
    }
    tab2() {
        this.setState({tab: 0});
      }
    tab1() {
        this.setState({tab: 1});
      }

            
    render() {
        return(
            <div className="container">
                <div className="row">
                    <div className="col s6 offset-s3 " >
                    <Inventory data={this.state.data}/>
                    </div>
                </div>
            </div>
            

        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'))