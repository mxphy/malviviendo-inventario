import React from 'react';
import Slot from './Slot';
import Rows from './Rows';
import { DragDropContext } from 'react-beautiful-dnd';


function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
class Inventory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            items: [],
            slotspp: 24,
            ffalse: false,
            ttrue: true,
            rows: null
        };
        this.onDragEnd = this.onDragEnd.bind(this);
    }
    componentWillReceiveProps(newProps) {
        this.setState({items: newProps.data});
    }
    outerHeight(el) {
        var height = el.offsetHeight;
        var style = getComputedStyle(el);
      
        height += parseInt(style.marginTop) + parseInt(style.marginBottom);
        return height;
      }

    toCenter() {
        var mainH = this.outerHeight(document.getElementById('root'));
        console.log('Main:'+mainH)
        var accountH = this.outerHeight(document.getElementById('panel'));
        console.log('Pannel:'+accountH)
        var marginT = (mainH - accountH) / 2;
        console.log(marginT)
        if (marginT > 30) {
            document.getElementById('panel').style.marginTop = ""+marginT - 15+"px";
        } else {
            document.getElementById('panel').style.marginTop = "30px";
        }
        console.log('Llamando2')
    }
    updateDimensions() {
        clearTimeout(this.timer);
        this.timer = setTimeout(this.toCenter(), 500);
        console.log('Llamando')
    }
    componentDidMount() {
        
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
        if(!isEmpty(this.state.items)) {
            this.createRows();
        }
        
        
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions.bind(this));
    }
    onDragEnd (result) {
        const {destination, source, draggableId } = result;

        if(!destination) {
            return;
        }
        if (
            destination.droppableId === source.droppableId &&
            destination.index === source.index
        ) {
            return;
        }
        const start = source.droppableId.split("-").pop();
        const final = destination.droppableId.split("-").pop();
        for (var x=0; x < this.state.items.length; x++)
            {
                let idd = parseInt(start);
                let itemmm = parseInt(this.state.items[x].Slot);
                if(itemmm === idd)
                {
                    let recipesCopy = JSON.parse(JSON.stringify(this.state.items))
                    
                    recipesCopy[x].Slot = parseInt(final); 
                    this.setState({
                        items:recipesCopy 
                        });
                    mp.trigger('onSlotChange', this.state.items[x].Name, parseInt(final))
                }
                else if (itemmm === parseInt(final))
                {
                    let recipesCopy = JSON.parse(JSON.stringify(this.state.items))
                    
                    recipesCopy[x].Slot = parseInt(start); 
                    this.setState({
                        items:recipesCopy 
                        });
                }
            }
            
    }
    createRows() {
        
        var rows = [];
        for (var i = 0; i < this.state.slotspp; i++) {
            let item = {};
            for (var x=0; x < this.state.items.length; x++)
            {
                if(this.state.items[x].Slot === i)
                {
                    item = this.state.items[x];

                }
            }
            if(!isEmpty(item))
            {
                let object = JSON.parse(JSON.stringify(item));
                rows.push(<Slot key={i} slotid={i} page={this.state.page} empty={this.state.ffalse} itemName={item.Name} object={object} itemQuantity={item.Quantity} itemDescription={item.Description} itemProperties={item.Properties}/>);
            }
            else {
                rows.push(<Slot key={i} slotid={i} page={this.state.page} empty={this.state.ttrue} />);
            }
        }
        return rows;
        
    }
    
    render() {
        
        return (
            <div className="card-panel teal grey darken-3" id="panel">
                <div className="card-title"><h4 className="white-text center-align" style={{marginTop:"0px", marginBottom:"0px"}}>Inventario</h4></div>
                <div className="row">
                    <br/>
                    <hr />
                    <br/>
                    
                    <div className="col s12">
                        <div className="row">
                            <DragDropContext onDragEnd={this.onDragEnd}>
                                {this.createRows()}
                            </DragDropContext>  
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}
export default Inventory