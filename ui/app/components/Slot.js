import React from 'react';
import { Droppable } from 'react-beautiful-dnd';
import Item from './Item';
class Slot extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dropid: this.props.slotid,
            empty: true,
            item: {}
        }
    }

    componentDidMount() {
        this.setState({
            dropid: this.props.slotid,
            empty: this.props.empty,
            item: {
                name: this.props.itemName,
                amount: this.props.itemQuantity,
                desc: this.props.itemDescription
            }
        });

    }

    deleteObject(name) {
        console.log("Borrado")
    }

    render() {
        return (
            <div>
            {this.props.empty ? (
                <Droppable droppableId={"slot-"+this.state.dropid}>
                {(provided) => (
                    <div className="col s2 card-rel"
                    {...provided.droppableProps}
                    ref={provided.innerRef}>
                    
                    <div className="card-panel teal grey darken-1 hoverable card-rel">
                        
                    </div>
                    
                    {provided.placeholder}
                    </div>
                )}
                
                </Droppable>
            ) : (

                <Droppable droppableId={"slot-"+this.state.dropid} isDropDisabled={!this.props.empty}>
                {(provided) => (
                    <div className="col s2 card-rel"
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    >
                    
                    <div className="card-panel teal grey darken-1 hoverable card-rel">
                    <Item slotid={this.props.slotid} empty={this.props.empty} deleteObject={this.deleteObject.bind(this)} itemName={this.props.itemName} itemDesc={this.props.itemDescription} itemQuantity={this.props.itemQuantity} itemProperties={this.props.itemProperties}/>
                    </div>
                    <span className="labitem badge blue white-text" style={{ minWidth:"0rem", width:"22px", borderRadius:"10px"}}>{this.props.itemQuantity}</span>
                    {provided.placeholder}
                    </div>
                )}
                
                </Droppable>
            )}
            
            </div>
        )
    }
}
export default Slot