import React from 'react';
import { Droppable } from 'react-beautiful-dnd';
import Item from './Item';
class Slot extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
            <Droppable droppableId="1">
            {(provided, snapshot) => {
                <div className="col s2"
                ref={provided.innerRef}
                {...provided.droppableProps}>
                <Item slotid={this.props.slotid}/>
                {provided.placeholder}
                </div>
            }}
            
            </Droppable>
            </div>
        )
    }
}
export default Slot