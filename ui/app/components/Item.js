import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import plural from 'pluralize-es';
import {searchItem} from './../images'
class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dragid: this.props.slotid,
            empty: this.props.empty,
            itemName: this.props.itemName,
            itemQuantity: this.props.itemQuantity,
            itemDesc: this.props.itemDesc,
            image: "tuputamadre",
            droppable: true,
            usable: true,
            show: false,
            read: false
        }
        this.handleClick = this.handleClick.bind(this);
    }
    async deleteItem() {
        const dropped = await mp.trigger('onDropObject', this.props.itemName);
        if (dropped) {
            if (this.state.itemQuantity === 1)
            {
                this.props.deleteObject(this.state.itemName);
            }
            else {
              let newQuantity = this.state.itemQuantity-1;
              this.setState({
                  itemQuantity: newQuantity
              })
            }
            
        }
    }
    async showPlayerList() {
        const listed = await mp.trigger('onRequestPlayers', this.props.itemName);
        if (listed) {
            console.log('listed')
            const {value: player} = await Swal.fire({
                title: '¿A quien deseas mostrar este objeto?',
                input: 'select',
                inputOptions: {
                  'apples': 'Jim_Street',
                  'bananas': 'Bananas',
                  'grapes': 'Grapes',
                  'oranges': 'Oranges'
                },
                inputPlaceholder: 'Selecciona un Usuario',
                showCancelButton: true,
                
              })
              
              if (player) {
                mp.trigger('onShowObject', this.props.itemName, player)
              }
            
        }
    }
    componentDidMount()
    {
        console.log('Propiedades '+ this.props.itemProperties)
        this.setState ({
            image: searchItem(this.state.itemName)
        })
        if(this.props.itemProperties === 0)
        {
            this.setState ({
                droppable: false,
                usable: false,
                show: false,
                read: false
            });
        }
        else if(this.props.itemProperties === 1)
        {
            this.setState ({
                droppable: true,
                usable: true,
                show: false,
                read: false
            });
        }
        else if(this.props.itemProperties === 2)
        {
            this.setState ({
                droppable: false,
                usable: false,
                show: true,
                read: true
            });
        }
        else {
            this.setState ({
                droppable: true,
                usable: true,
                show: false,
                read: false
            });
        }
    }
    handleClick (e) {
        e.preventDefault();
        const MySwal = withReactContent(Swal)
        const title = this.props.itemName+" ("+this.props.itemQuantity+")";
        if(this.state.droppable && this.state.usable)
        {
            MySwal.fire({
                title: title,
                imageUrl: this.state.image,
                imageWidth: 200,
                imageHeight: 200,
                imageAlt: title,
                width: 400,
                text: this.props.itemDesc,
                showCancelButton: true,
                confirmButtonText: 'Usar',
                cancelButtonText: 'Tirar'
              }).then((result) => {
                if (result.value) {
                    mp.trigger('onUseObject', this.props.itemName)
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    
                      this.deleteItem();
                }
              })
        }
        else if(this.state.show && this.state.read)
        {
            MySwal.fire({
                title: title,
                imageUrl: this.state.image,
                imageWidth: 200,
                imageHeight: 200,
                imageAlt: title,
                width: 400,
                text: this.props.itemDesc,
                showCancelButton: true,
                confirmButtonText: 'Leer',
                cancelButtonText: 'Mostrar'
              }).then((result) => {
                if (result.value) {
                    mp.trigger('onReadObject', this.props.itemName)
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    
                      this.showPlayerList();
                }
              })
        }
        else {
            MySwal.fire({
                title: title,
                imageUrl: this.state.image,
                imageWidth: 200,
                imageHeight: 200,
                imageAlt: title,
                width: 400,
                text: this.props.itemDesc,
              })
        }
        

    }
    render() {
        return (
            <div onContextMenu={this.handleClick}>
                <Draggable draggableId={"drag-"+this.props.slotid} index={1}>
                    {(provided, ) => (
                        <div ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}>
                        
                        <img src={this.state.image} className="itemabs" />
                        
                        
                        </div>
                    )}
                </Draggable>

            </div>
        )
    }
}
export default Item