
const info = {
    "CIGARRO" : "https://cdn.discordapp.com/emojis/520865954486550548.png",
    "BOTIQUIN" : "https://cdn.discordapp.com/emojis/463712557870481428.png"
    };
export function searchItem(item) {
    if(info[item] === undefined) {
        return "https://discordapp.com/assets/209381ec0f39a61c1904269ed41c62eb.svg"
    }
    return info[item]
}